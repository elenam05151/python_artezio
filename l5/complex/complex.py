import math

class Complex(object):

    def __init__(self, ab_string):
        arr = ab_string.split(" ")
        self.re = round(float(arr[0]),2)
        self.im = round(float(arr[1]),2)
        self.val = str(self.re)+'+'+str(self.im)+'i'
    
    def __add__(self, other):
        re = self.re + other.re
        im = self.im + other.im
        string = str(re)+' '+str(im)
        return Complex(string)

    def __sub__(self, other):
        re = self.re - other.re
        im = self.im - other.im
        string = str(re)+' '+str(im)
        return Complex(string)

    def __mul__(self, other):
        re = self.re*other.re - self.im*other.im
        im = self.re*other.im + self.im*other.re
        string = str(re)+' '+str(im)
        return Complex(string)

    def __truediv__(self, other):
        re = (self.re*other.re + self.im*other.im)/(other.im*other.im+other.re*other.re)
        im = (other.re*self.im - other.im*self.re)/(other.im*other.im+other.re*other.re)
        string = str(re)+' '+str(im)
        return Complex(string)

    def __abs__(self):
        re = math.sqrt(self.re*self.re + self.im*self.im)
        im = 0.0
        string = str(re)+' '+str(im)
        return Complex(string)


comp1 = Complex("2 1")
comp2 = Complex("5 6")
print(comp1.re, comp1.im, comp1.val)
print(comp2.re, comp2.im, comp2.val)
comp3 = comp1+comp2
print(comp3.val)
comp4 = comp1-comp2
print(comp4.val)
comp5 = comp1*comp2
print(comp5.val)
comp6 = comp1/comp2
print(comp6.val)

comp7 = abs(comp1)
print(comp7.val)
comp8 = abs(comp2)
print(comp8.val)