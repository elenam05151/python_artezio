import numpy as np

class Student(object):

    def __init__(self, name, assessments_dict = {"exam_max": 30, "lab_max": 7, "lab_num": 10, "k": 0.0}):
        self.assessments_dict = assessments_dict
        self.labs_arr = np.zeros(assessments_dict["lab_num"])
        self.points = 0.0
        self.exam = 0.0

    def make_lab(self,m,n = 1):
        max_ass = self.assessments_dict["lab_max"]

        if (n > self.assessments_dict["lab_num"]):
            print("additional points for extra laboratory")
            for i in range (self.assessments_dict["lab_num"], n+1):
                self.labs_arr = np.append(self.labs_arr, 0)
            #print(self.labs_arr)
            #self.assessments_dict["lab_num"] = n
            #self.labs_arr[n] = m

        if (m > max_ass):
            self.labs_arr[n-1] = max_ass
        elif (m < 0):
            print("assessment is not correct")
        else:
            self.labs_arr[n-1] = m
        print("Labs points: ", self.labs_arr)
        self.points = sum(self.labs_arr)
        print("Sum of point: ", self.points)

    def make_exam(self,m):
        max_ass = self.assessments_dict["exam_max"]
        if (m > max_ass):
            self.exam = 30.0
        elif (m < 0):
            print("assessments is not correct")
        else:
            self.exam = m
        print("Exam point: ", self.exam)

    def is_certified(self):
        min_point = 40
        sum_point = self.points + self.exam
        if sum_point > min_point:
            return True, sum_point
        else:
            return False, sum_point

ivan = Student("Ivan")
ivan.make_lab(7)
ivan.make_lab(2)
ivan.make_lab(5, 10)
ivan.make_lab(5, 16)
ivan.make_exam(20)
print("Ivan results: labs", ivan.points," ; exam: ", ivan.exam)

print("Ivan certification status, points: ", ivan.is_certified())

ivan.make_exam(30)
print("Ivan certification status, points: ", ivan.is_certified())