#range()

def range_function(a , b = 0):
    if b > a:
      c = b
      b = a
      a = c
    arr = []
    for i in range(b, a+1):
       arr.append(i)
    return arr

a = 10
print("range: ", a)
list = range_function(a)
print(list)