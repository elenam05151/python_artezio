#Factorial function

def factorial(a):
    if a == 0:
        return 1
    ai = 1
    fact = 1
    while ai < a+1:
        fact = fact*ai
        ai = ai+1
    return fact

var = 4
print("A = ", var)
f = factorial(var)
print("fact A = ", f)