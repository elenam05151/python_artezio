sum = 0

def adder(*nums, **knums):
    global sum
    for n in nums:
        if(n == None):
            return None
        try:
            sum += n
        except:
            adder(*n)
            print("recursive run...")

    for key, value in knums.items():
        try:
            #print(value)
            sum += value
        except:
            adder(*value)
            print("recursive run...")
    #print("Sum: ", sum)
    return sum

sum = adder(1, 2, [3, 4, (5, 6, 0)], k = 5, a=(10, 11), b=(3, 4, [5, 6, [7, 8], []]))

#a = [1, 2, 3]
#b = a.append(a)
#print(b)
#sum = adder(b)
print ("Sum: ", sum)