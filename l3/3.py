
def foo(a,b,c,d):
    global max_ever
    aver = (a+b+c+d)/4
    max_local = max(a,b,c,d)
    if (max_ever < max_local):
        max_ever = max_local
    return aver, max_ever

a = 1
b = 2
c = 3
d = -4
max_ever = max(a,b,c,d)
aver, max_ever = foo(a,b,c,d)
print(aver, max_ever)
aver, max_ever = foo(1,2,3,4) 
print("foo(1,2,3,4) -> ", aver, max_ever)
aver, max_ever = foo (-3, -2, 10, 1) 
print("foo(-3, -2, 10, 1) -> ", aver, max_ever)
aver, max_ever = foo(7,8,8,1) 
print("foo(7,8,8,1) -> ", aver, max_ever)