from functools import reduce

def close_to_zero(a):
    arr = a.split()
    print(arr)
    almost_zero = float(arr[0])
    for num in arr:
        if abs(float(num)) < abs(almost_zero):
            almost_zero = float(num)
    return almost_zero

def close_to_zero_short(a):
    arr = a.split()
    print(arr)
    f = reduce(lambda a,b: float(a) if abs(float(a)) < abs(float(b)) else b, arr, arr[0])
    return f

a = input()
#a = "0.8 0.7 9 -0.5"
print(a)
almost_zero = close_to_zero_short(a) 
print(almost_zero)
