
def sqr_for_list(input_list):
    sqr_list = []
    for a in input_list:
        sqr_list.append(a*a)
    return sqr_list

def even_positions_list(input_list):
    even_list = []
    i = 1
    while i < len(input_list):
      even_list.append(input_list[i])
      i = i+2
    return even_list

def even_elements_odd_positions_list(input_list):
    out_list = []
    i = 0
    while i < len(input_list):
      if (input_list[i] % 2) == 0:
          out_list.append(input_list[i])
      i = i+2
    return out_list

list =  [2,2,3,5]
print(list)
sqr_list = sqr_for_list(list)
print(sqr_list)

even_list = even_positions_list(list)
print(even_list)

sort_list = even_elements_odd_positions_list(list)
print(sort_list)