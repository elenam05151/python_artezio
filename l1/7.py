#script to merge two Python dictionaries

a = {1:'one',2:'two'}
b = {3:'three',4:'four'}
print('a = ', a)
print('b = ', b)
c = a
c.update(b)
print('a+b = ')
print(c)