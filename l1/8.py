#program to find the highest 3 values in a dictionary
from collections import Counter 
  
dict = {0: 44, 1: 30, 2: 4, 3: 1, 4: 122, 5: 37, 6: 7, 7: 93} 
print(dict)

dict_high = Counter(dict).most_common(3)  
for i in dict_high: 
    print(i[0],":",i[1]," ") 
