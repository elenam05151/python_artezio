from itertools import chain
sa = set([1,2])
sb = set([2,3]) 
sc = set([2])
print(sa, sb)

sa_new = set(chain(sa,sc))
sb_new = set(chain(sb,sc))
print(sa, sb)

if (len(sa_new)==len(sa))and(len(sb_new)==len(sb)):
    print(True)
else:
    print(False)