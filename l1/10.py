#program to get the difference between the two lists

list_1 = ['a', 10, 20, 30, 30, 40, 40]
list_2 = ['b', 18, 20, 30]

set_1 = set(list_2)
if len(list_1) == len(set_1):
    print(set(list_1)-set(list_2))
else:
    list_12 = list_1 + list_2
    list_dif = [i for i in list_12 if i not in list_2]
    print(list_dif)	